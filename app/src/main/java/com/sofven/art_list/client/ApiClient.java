package com.sofven.art_list.client;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ApiClient {

    private static final String TAG = "API_CLIENT";

    private Context context;

    private static final String API_BASE_URL = "https://api.art-list.com/api";
    private static final String API_VERSIONED_URL = API_BASE_URL + "/v1";
    private static final String API_PUBLIC_URL = API_VERSIONED_URL + "/public";

    private static final String PUBLIC_GET_POSTS_URL = API_PUBLIC_URL + "/posts";
    private static final String PUBLIC_GET_POST_URL = PUBLIC_GET_POSTS_URL + "/{slug}";
    private static final String PUBLIC_REACT_TO_POST_URL = PUBLIC_GET_POSTS_URL + "/{slug}/{reaction}";

    private static final String PUBLIC_GET_TERMS_URL = API_PUBLIC_URL + "/terms";
    private static final String PUBLIC_GET_TERM_URL = PUBLIC_GET_TERMS_URL + "/{slug}";
    private static final String PUBLIC_GET_TERM_POSTS_URL = PUBLIC_GET_TERM_URL + "/posts";

    public ApiClient(Context activityContext) {
        context = activityContext;
    }

    private void doRequest(final int method,
                          final String url,
                          final Map<String, String> header,
                          final Map<String, String> urlVars,
                          final Map<String, String> urlParams,
                          final Map<String, String> data,
                          Listener<String> responseListener,
                          ErrorListener errorListener) {

        RequestQueue queue = Volley.newRequestQueue(context);
        String endpoint = url;

        if (urlParams != null) {
            Uri.Builder uriBuilder = Uri.parse(endpoint).buildUpon();
            for (Map.Entry<String, String> entry : urlParams.entrySet()) {
                uriBuilder.appendQueryParameter(entry.getKey(), entry.getValue());
            }
            try {
                URL urlBuilt = new URL(uriBuilder.toString());
                endpoint = urlBuilt.toString();
            } catch(MalformedURLException e) {
                Log.d(TAG, e.getMessage());
            }
        }

        if (urlVars != null) {
            for (Map.Entry<String, String> entry : urlVars.entrySet()) {
                endpoint = endpoint.replace("{" + entry.getKey() + "}", entry.getValue());
            }
        }
        StringRequest request = new StringRequest(method, endpoint, responseListener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                if (header == null) {
                    return new HashMap<>();
                }
                return header;
            }
            @Override
            protected Map<String, String> getParams() {
                return data;
            }
        };
        queue.add(request);
    }

    public void getPosts(final Map<String, String> urlParams, Listener<String> responseListener, ErrorListener errorListener) {
        Map<String, String> urlVars = new HashMap<>();
        doRequest(Request.Method.GET, PUBLIC_GET_POSTS_URL, null, urlVars, urlParams, null, responseListener, errorListener);
    }

    public void getPost(final String slug, Listener<String> responseListener, ErrorListener errorListener) {
        Map<String, String> urlVars = new HashMap<>();
        urlVars.put("slug", slug);

        doRequest(Request.Method.GET, PUBLIC_GET_POST_URL, null, urlVars, null, null, responseListener, errorListener);
    }

    public void postReact(final String slug, final String reaction, Listener<String> responseListener, ErrorListener errorListener) {
        Map<String, String> urlVars = new HashMap<>();
        urlVars.put("slug", slug);
        urlVars.put("reaction", reaction);

        doRequest(Request.Method.POST, PUBLIC_REACT_TO_POST_URL, null, urlVars, null, null, responseListener, errorListener);
    }

    public void getTerms(final Map<String, String> urlParams, Listener<String> responseListener, ErrorListener errorListener) {
        Map<String, String> urlVars = new HashMap<>();
        doRequest(Request.Method.GET, PUBLIC_GET_TERMS_URL, null, urlVars, urlParams, null, responseListener, errorListener);
    }

    public void getTerm(final String slug, Listener<String> responseListener, ErrorListener errorListener) {
        Map<String, String> urlVars = new HashMap<>();
        urlVars.put("slug", slug);

        doRequest(Request.Method.GET, PUBLIC_GET_TERM_URL, null, urlVars, null, null, responseListener, errorListener);
    }

    public void getTermPosts(final String slug, final Map<String, String> urlParams, Listener<String> responseListener, ErrorListener errorListener) {
        Map<String, String> urlVars = new HashMap<>();
        urlVars.put("slug", slug);

        doRequest(Request.Method.GET, PUBLIC_GET_TERM_POSTS_URL, null, urlVars, urlParams, null, responseListener, errorListener);
    }
}
