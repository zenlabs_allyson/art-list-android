package com.sofven.art_list.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Post {

    @SerializedName("id")
    private Long id;

    @SerializedName("user")
    private PostUser user;

    @SerializedName("name")
    private String name;

    @SerializedName("slug")
    private String slug;

    @SerializedName("content")
    private String content;

    @SerializedName("files")
    private List<PostFile> files;

    @SerializedName("terms")
    private List<Term> terms;

    @SerializedName("created_at")
    private TimeStamp createdAt;

    @SerializedName("updated_at")
    private TimeStamp updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PostUser getUser() {
        return user;
    }

    public void setUser(PostUser user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<PostFile> getFiles() {
        return files;
    }

    public void setFiles(List<PostFile> files) {
        this.files = files;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }

    public TimeStamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(TimeStamp createdAt) {
        this.createdAt = createdAt;
    }

    public TimeStamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(TimeStamp updatedAt) {
        this.updatedAt = updatedAt;
    }

}
