package com.sofven.art_list.model;

import com.google.gson.annotations.SerializedName;

public class Term {

    @SerializedName("id")
    private Long user;

    @SerializedName("name")
    private String name;

    @SerializedName("slug")
    private String slug;

    @SerializedName("type")
    private String type;

    @SerializedName("thumb")
    private PostFile thumb;

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PostFile getThumb() {
        return thumb;
    }

    public void setThumb(PostFile thumb) {
        this.thumb = thumb;
    }
}
