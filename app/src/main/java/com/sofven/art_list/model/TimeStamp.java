package com.sofven.art_list.model;

import com.google.gson.annotations.SerializedName;

public class TimeStamp {

    @SerializedName("date")
    private String date;

    @SerializedName("timezone_type")
    private Integer timezoneType;

    @SerializedName("timezone")
    private String timezone;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getTimezoneType() {
        return timezoneType;
    }

    public void setTimezoneType(Integer timezoneType) {
        this.timezoneType = timezoneType;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
