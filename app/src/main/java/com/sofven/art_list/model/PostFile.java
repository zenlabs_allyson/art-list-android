package com.sofven.art_list.model;

import com.google.gson.annotations.SerializedName;

public class PostFile {

    @SerializedName("id")
    private Long id;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("file")
    private String file;

    @SerializedName("thumb")
    private String thumb;

    @SerializedName("mime")
    private String mime;

    @SerializedName("created_at")
    private TimeStamp createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public TimeStamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(TimeStamp createdAt) {
        this.createdAt = createdAt;
    }
}
