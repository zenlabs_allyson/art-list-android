package com.sofven.art_list.enums;

public enum PostReaction {
    like,
    dislike
}
