package com.sofven.art_list.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sofven.art_list.activity.PostActivity;
import com.sofven.art_list.client.ApiClient;
import com.sofven.art_list.enums.PostReaction;
import com.sofven.art_list.enums.PreferenceName;
import com.sofven.art_list.enums.ResponseStatus;
import com.sofven.art_list.listeners.OnBottomReachedListener;
import com.sofven.art_list.model.Post;
import com.sofven.art_list.R;
import com.sofven.art_list.model.PostFile;
import com.sofven.art_list.result.RestResult;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {

    public static final String EXTRA_POST_SLUG = "POST_SLUG";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ApiClient apiClient;

    private Context mContext;
    private List<Post> posts;
    private Boolean usingSearchLayout = false;
    private Resources res;
    private OnBottomReachedListener onBottomReachedListener;

    private String reaction;

    public PostAdapter(Context context, List<Post> posts) {
        mContext = context;
        this.posts = posts;
        this.res = mContext.getResources();

        // Initialize ApiClient
        apiClient = new ApiClient(mContext);

        // Initialize SharedPreferences
        preferences = mContext.getSharedPreferences(PreferenceName.POSTS_SETTINGS.name(), Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public PostAdapter(Context context, List<Post> posts, Boolean isSearch) {
        mContext = context;
        this.posts = posts;
        this.res = mContext.getResources();
        this.usingSearchLayout = isSearch;

        // Initialize ApiClient
        apiClient = new ApiClient(mContext);

        // Initialize SharedPreferences
        preferences = mContext.getSharedPreferences(PreferenceName.POSTS_SETTINGS.name(), Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public FlexboxLayout files;
        public TextView name, content;
        public ImageView react;
        public View view;

        public MyViewHolder(View view) {
            super(view);
            this.view = view;
            if (usingSearchLayout) {
                name = view.findViewById(R.id.text_view_card_search_name);
                content = view.findViewById(R.id.text_view_card_search_content);
                files = view.findViewById(R.id.layout_card_search_files);
                react = view.findViewById(R.id.image_button_card_search_react);
            } else {
                name = view.findViewById(R.id.text_view_card_post_name);
                content = view.findViewById(R.id.text_view_card_post_content);
                files = view.findViewById(R.id.layout_card_post_files);
                react = view.findViewById(R.id.image_button_card_post_react);
            }
        }
    }

    @Override
    public PostAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_post, parent, false);
        if (usingSearchLayout) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_search, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PostAdapter.MyViewHolder holder, int position) {
        final Post post = posts.get(position);
        holder.name.setText(post.getName());
        holder.content.setText(post.getContent());

        Boolean liked = preferences.getBoolean(PostReaction.like.name() + post.getId(), false);
        if (liked) {
            holder.react.setImageResource(R.drawable.ic_like_filled);
        } else {
            holder.react.setImageResource(R.drawable.ic_like);
        }

        holder.react.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean liked = preferences.getBoolean(PostReaction.like.name() + post.getId(), false);
                if (liked) {
                    reaction = PostReaction.dislike.name();
                } else {
                    reaction = PostReaction.like.name();
                }
                apiClient.postReact(post.getSlug(), reaction,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Convert json string to object
                            Gson gson = new GsonBuilder().create();
                            RestResult result = gson.fromJson(response, RestResult.class);
                            if (result.getStatus() != ResponseStatus.success) {
                                Toast.makeText(mContext, result.getMessage(), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            // Success
                            if (reaction.equals(PostReaction.like.name())) {
                                holder.react.setImageResource(R.drawable.ic_like_filled);
                                editor.putBoolean(PostReaction.like.name() + post.getId(), true).commit();
                                reaction = PostReaction.dislike.name();
                            } else {
                                holder.react.setImageResource(R.drawable.ic_like);
                                editor.remove(PostReaction.like.name() + post.getId()).commit();
                                reaction = PostReaction.like.name();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(mContext, mContext.getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    });
            }
        });

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PostActivity.class);
                intent.putExtra(EXTRA_POST_SLUG, post.getSlug());
                mContext.startActivity(intent);
            }
        };

        holder.files.setOnClickListener(clickListener);
        holder.name.setOnClickListener(clickListener);
        holder.content.setOnClickListener(clickListener);

        holder.files.removeAllViews();
        int filesCount = post.getFiles().size();
        if (filesCount > 0) {
            int index = 1;
            for (PostFile file : post.getFiles()) {
                ImageView image = new ImageView(mContext);
                LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                if (usingSearchLayout) {
                    imageParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                } else {
                    imageParams.setMargins(0, 0, 0, res.getDimensionPixelSize(R.dimen.spacing_one));
                }
                String filePath = file.getFile();
                if (usingSearchLayout) {
                    filePath = file.getThumb();
                }

                image.setLayoutParams(imageParams);
                image.setAdjustViewBounds(true);
                Glide.with(mContext)
                    .load(filePath)
                    //.placeholder(R.drawable.placeholder)
                    .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                    .into(image);

                int imageLimit = 6;
                if (usingSearchLayout) {
                    image.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    if (index == 4) {
                        break;
                    }
                }
                if (index == imageLimit && filesCount > imageLimit) {
                    RelativeLayout rLayout = new RelativeLayout(mContext);
                    LinearLayout.LayoutParams rlParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Resources.getSystem().getDisplayMetrics().widthPixels / 2);
                    if (usingSearchLayout) {
                        rlParams = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    }
                    rLayout.setLayoutParams(rlParams);

                    TextView textRemaining = new TextView(mContext);
                    RelativeLayout.LayoutParams tParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, Resources.getSystem().getDisplayMetrics().widthPixels / 2);
                    if (usingSearchLayout) {
                        tParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                    }
                    textRemaining.setLayoutParams(tParams);
                    textRemaining.setText(String.format(res.getString(R.string.plus_number), filesCount - imageLimit + 1));
                    textRemaining.setTextSize(TypedValue.COMPLEX_UNIT_PX, res.getDimension(R.dimen.font_size_header_one));
                    textRemaining.setTextColor(res.getColor(R.color.colorPrimary));
                    textRemaining.setBackgroundColor(res.getColor(R.color.blackTrans80));
                    textRemaining.setGravity(Gravity.CENTER);

                    image.setLayoutParams(tParams);

                    rLayout.addView(image);
                    rLayout.addView(textRemaining);

                    holder.files.addView(rLayout);
                    break;
                } else {
                    holder.files.addView(image);
                }
                index++;
            }
        }

        if (position == posts.size() - 1) {
            onBottomReachedListener.onBottomReached(position);
        }
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){
        this.onBottomReachedListener = onBottomReachedListener;
    }
}