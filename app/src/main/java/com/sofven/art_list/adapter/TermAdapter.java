package com.sofven.art_list.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sofven.art_list.R;
import com.sofven.art_list.activity.TermPostsActivity;
import com.sofven.art_list.listeners.OnBottomReachedListener;
import com.sofven.art_list.model.Term;

import java.util.List;

public class TermAdapter extends RecyclerView.Adapter<TermAdapter.MyViewHolder> {

    public static final String EXTRA_TERM_TYPE = "TERM_TYPE";
    public static final String EXTRA_TERM_SLUG = "TERM_SLUG";

    private Context mContext;
    private List<Term> terms;
    private Resources res;
    private OnBottomReachedListener onBottomReachedListener;

    public TermAdapter(Context context, List<Term> terms) {
        mContext = context;
        this.terms = terms;
        this.res = mContext.getResources();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public ImageView image;
        public View view;

        public MyViewHolder(View view) {
            super(view);
            this.view = view;
            name = view.findViewById(R.id.text_view_term_name);
            image = view.findViewById(R.id.image_view_term_image);
        }
    }

    @Override
    public TermAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_term, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TermAdapter.MyViewHolder holder, int position) {
        final Term term = terms.get(position);
        holder.name.setText(term.getName());
        try {
            Glide.with(mContext)
                .load(term.getThumb().getThumb())
                .thumbnail(Glide.with(mContext).load(R.drawable.loading))
                .into(holder.image);
        } catch (Exception e) {
            holder.image.setImageResource(R.drawable.placeholder);
        }
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, TermPostsActivity.class);
                intent.putExtra(EXTRA_TERM_TYPE, term.getType());
                intent.putExtra(EXTRA_TERM_SLUG, term.getSlug());
                mContext.startActivity(intent);
            }
        });

        if (position == terms.size() - 1) {
            onBottomReachedListener.onBottomReached(position);
        }
    }

    @Override
    public int getItemCount() {
        return terms.size();
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener) {
        this.onBottomReachedListener = onBottomReachedListener;
    }
}