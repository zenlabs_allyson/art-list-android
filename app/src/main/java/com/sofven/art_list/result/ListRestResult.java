package com.sofven.art_list.result;

import com.google.gson.annotations.SerializedName;

public class ListRestResult extends RestResult {

    @SerializedName("count")
    private Long count;

    @SerializedName("total")
    private Long total;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
