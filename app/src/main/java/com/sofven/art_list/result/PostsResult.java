package com.sofven.art_list.result;

import com.google.gson.annotations.SerializedName;
import com.sofven.art_list.model.Post;

import java.util.List;

public class PostsResult extends ListRestResult {

    @SerializedName("posts")
    private List<Post> posts;

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
