package com.sofven.art_list.result;

import com.google.gson.annotations.SerializedName;
import com.sofven.art_list.model.Post;

public class PostResult extends RestResult {

    @SerializedName("post")
    private Post post;

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
