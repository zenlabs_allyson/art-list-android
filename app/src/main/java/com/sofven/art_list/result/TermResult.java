package com.sofven.art_list.result;

import com.google.gson.annotations.SerializedName;
import com.sofven.art_list.model.Term;

public class TermResult extends RestResult {

    @SerializedName("term")
    private Term term;

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }
}
