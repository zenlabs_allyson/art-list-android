package com.sofven.art_list.result;

import com.google.gson.annotations.SerializedName;
import com.sofven.art_list.model.Term;

import java.util.List;

public class TermsResult extends ListRestResult {

    @SerializedName("terms")
    private List<Term> terms;

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }
}
