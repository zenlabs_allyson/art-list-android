package com.sofven.art_list.result;

import com.google.gson.annotations.SerializedName;
import com.sofven.art_list.enums.ResponseStatus;

public class RestResult {

    @SerializedName("status")
    private ResponseStatus status;

    @SerializedName("message")
    private String message;

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
