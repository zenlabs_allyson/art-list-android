package com.sofven.art_list.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sofven.art_list.R;
import com.sofven.art_list.adapter.TermAdapter;
import com.sofven.art_list.client.ApiClient;
import com.sofven.art_list.enums.PostTermType;
import com.sofven.art_list.enums.ResponseStatus;
import com.sofven.art_list.listeners.OnBottomReachedListener;
import com.sofven.art_list.util.NetworkUtil;
import com.sofven.art_list.model.Term;
import com.sofven.art_list.result.TermsResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArtistFragment extends Fragment {

    private static final String TAG = "FRAGMENT_ARTIST";
    private static final String NAME = "Artists";

    private Context mContext;
//    private SharedPreferences preferences;
    private NetworkUtil networkUtil;
    private ApiClient apiClient;
    private TermAdapter adapter;

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView message;

    private List<Term> terms = new ArrayList<>();
    private Map<String, String> data = new HashMap<>();
    private int offset = 0;
    private int limit = 10;
    private boolean endOfList = false;
    private String search;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            search = getArguments().getString("search");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_artist, container, false);

        mContext = getActivity();

        // Initialize NetworkUtil class
        networkUtil = new NetworkUtil(mContext);

        message = rootView.findViewById(R.id.text_view_artists_message);

        // Initialize ApiClient
        apiClient = new ApiClient(mContext);

        recyclerView = rootView.findViewById(R.id.recycler_view_artists);

        adapter = new TermAdapter(mContext, terms);

        final RecyclerView.LayoutManager manager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        adapter.setOnBottomReachedListener(new OnBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                addTerms();
            }
        });

        swipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh_artists);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initializeTerm();
            }
        });
        initializeTerm();

        return rootView;
    }

    private void initializeTerm() {
        offset = 0;
        terms.clear();
        endOfList = false;

        swipeRefreshLayout.setRefreshing(true);

        if (search != null && !search.isEmpty()) {
            data.put("search", search);
        }

        data.put("limit", Integer.toString(limit));
        data.put("orderBy", "name");
        data.put("order", "asc");
        data.put("type", PostTermType.artists.name());

        addTerms();
    }

    private void addTerms() {
        if (!endOfList) {
            message.setText(R.string.loading_artists);
            message.setVisibility(View.VISIBLE);

            Log.d(TAG, "Adding more terms. Offset is " + offset);

            final View.OnClickListener onErrorClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addTerms();
                }
            };

            // Check if network is available using NetworkUtil class
            if (!networkUtil.isNetworkAvailable()) {
                swipeRefreshLayout.setRefreshing(false);
                message.setText(R.string.no_network);
                message.setVisibility(View.VISIBLE);
                message.setOnClickListener(onErrorClickListener);
                return;
            }
            message.setOnClickListener(null);

            data.put("offset", Integer.toString(offset));
            apiClient.getTerms(data,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Convert json string to object
                        Gson gson = new GsonBuilder().create();
                        TermsResult result = gson.fromJson(response, TermsResult.class);
                        // If not successful
                        if (result.getStatus() != ResponseStatus.success) {
                            message.setText(result.getMessage());
                            message.setVisibility(View.VISIBLE);
                            return;
                        }
                        message.setOnClickListener(null);
                        // Successful
                        if (result.getTerms().size() == 0) {
                            endOfList = true;
                            message.setText(R.string.no_more_artists);
                            message.setVisibility(View.VISIBLE);
                            return;
                        }
                        message.setText("");
                        message.setVisibility(View.INVISIBLE);
                        terms.addAll(result.getTerms());
                        adapter.notifyDataSetChanged();
                        offset = offset + limit + 1;
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        message.setText(getString(R.string.error_occurred));
//                        message.setVisibility(View.VISIBLE);
//                        message.setOnClickListener(onErrorClickListener);
                    }
                });
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    public void refreshAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

}
