package com.sofven.art_list.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sofven.art_list.R;
import com.sofven.art_list.adapter.PostAdapter;
import com.sofven.art_list.client.ApiClient;
import com.sofven.art_list.enums.PostOrder;
import com.sofven.art_list.enums.PostsSettings;
import com.sofven.art_list.enums.PreferenceName;
import com.sofven.art_list.enums.ResponseStatus;
import com.sofven.art_list.util.NetworkUtil;
import com.sofven.art_list.listeners.OnBottomReachedListener;
import com.sofven.art_list.model.Post;
import com.sofven.art_list.result.PostsResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class FeedFragment extends Fragment {

    private static final String TAG = "FRAGMENT_FEED";
    private static final String NAME = "Feed";

    private Context mContext;
    private SharedPreferences preferences;
    private NetworkUtil networkUtil;
    private ApiClient apiClient;
    private PostAdapter adapter;

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView message;

    private List<Post> posts = new ArrayList<>();
    private Map<String, String> data = new HashMap<>();
    private int offset = 0;
    private int limit = 10;
    private boolean endOfList = false;
    private String slug, search;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            slug = getArguments().getString("slug");
            search = getArguments().getString("search");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_feed, container, false);

        mContext = getActivity();

        // Initialize NetworkUtil class
        networkUtil = new NetworkUtil(mContext);

        message = rootView.findViewById(R.id.text_view_feed_message);

        // Initialize ApiClient
        apiClient = new ApiClient(mContext);

        recyclerView = rootView.findViewById(R.id.recycler_view_posts);

        if (search != null) {
            adapter = new PostAdapter(mContext, posts, true);
        } else {
            adapter = new PostAdapter(mContext, posts);
        }

        final RecyclerView.LayoutManager manager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        adapter.setOnBottomReachedListener(new OnBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                addPosts();
            }
        });

        swipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh_feed);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initializeFeed();
            }
        });
        initializeFeed();

        return rootView;
    }

    private void initializeFeed() {
        offset = 0;
        posts.clear();
        endOfList = false;

        swipeRefreshLayout.setRefreshing(true);

        // Initialize SharedPreferences
        preferences = mContext.getSharedPreferences(PreferenceName.POSTS_SETTINGS.name(), Context.MODE_PRIVATE);
        String postOrder = preferences.getString(PostsSettings.ORDER.name(), PostOrder.RANDOM.name());

        data.put("limit", Integer.toString(limit));
        if ((slug != null && !slug.isEmpty()) || (search != null && !search.isEmpty())) {
            if (search != null && !search.isEmpty()) {
                data.put("search", search);
            }
            data.put("orderBy", "id");
            data.put("order", "desc");
        } else {
            if (postOrder.equals(PostOrder.RECENT.name())) {
                data.put("orderBy", "id");
                data.put("order", "desc");
                data.remove("random_seed");
            } else {
                Random rand = new Random();
                data.put("random_seed", Integer.toString(rand.nextInt(999) + 100));
                data.remove("orderBy");
                data.remove("order");
            }
        }

        addPosts();
    }

    private void addPosts() {
        if (!endOfList) {
            message.setText(R.string.loading_feed);
            message.setVisibility(View.VISIBLE);

            Log.d(TAG, "Adding more posts. Offset is " + offset);

            final View.OnClickListener onErrorClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addPosts();
                }
            };

            // Check if network is available using NetworkUtil class
            if (!networkUtil.isNetworkAvailable()) {
                swipeRefreshLayout.setRefreshing(false);
                message.setText(R.string.no_network);
                message.setVisibility(View.VISIBLE);
                message.setOnClickListener(onErrorClickListener);
                return;
            }
            message.setOnClickListener(null);

            data.put("offset", Integer.toString(offset));
            if (slug != null && !slug.isEmpty()) {
                apiClient.getTermPosts(slug, data,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Convert json string to object
                            Gson gson = new GsonBuilder().create();
                            PostsResult result = gson.fromJson(response, PostsResult.class);
                            // If not successful
                            if (result.getStatus() != ResponseStatus.success) {
                                message.setText(result.getMessage());
                                message.setVisibility(View.VISIBLE);
                                return;
                            }
                            message.setOnClickListener(null);
                            // Successful
                            if (result.getPosts().size() == 0) {
                                endOfList = true;
                                message.setText(R.string.no_more_posts);
                                message.setVisibility(View.VISIBLE);
                                return;
                            }
                            message.setText("");
                            message.setVisibility(View.INVISIBLE);
                            posts.addAll(result.getPosts());
                            adapter.notifyDataSetChanged();
                            offset = offset + limit + 1;
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            message.setText(getString(R.string.error_occurred));
//                            message.setVisibility(View.VISIBLE);
//                            message.setOnClickListener(onErrorClickListener);
                        }
                    });
            } else {
                apiClient.getPosts(data,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Convert json string to object
                            Gson gson = new GsonBuilder().create();
                            PostsResult result = gson.fromJson(response, PostsResult.class);
                            // If not successful
                            if (result.getStatus() != ResponseStatus.success) {
                                message.setText(result.getMessage());
                                message.setVisibility(View.VISIBLE);
                                return;
                            }
                            message.setOnClickListener(null);
                            // Successful
                            if (result.getPosts().size() == 0) {
                                endOfList = true;
                                message.setText(R.string.no_more_posts);
                                message.setVisibility(View.VISIBLE);
                                return;
                            }
                            message.setText("");
                            message.setVisibility(View.INVISIBLE);
                            posts.addAll(result.getPosts());
                            adapter.notifyDataSetChanged();
                            offset = offset + limit + 1;
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            message.setText(getString(R.string.error_occurred));
//                            message.setVisibility(View.VISIBLE);
//                            message.setOnClickListener(onErrorClickListener);
                        }
                    });
            }
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    public void refreshAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

}
