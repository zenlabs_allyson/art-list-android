package com.sofven.art_list.util;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.sofven.art_list.R;

public class AnalyticsUtil {

    private GoogleAnalytics sAnalytics;
    private Tracker sTracker;

    public AnalyticsUtil(Context context) {
        sAnalytics = GoogleAnalytics.getInstance(context);
    }

    public Tracker getDefaultTracker() {
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }
}

