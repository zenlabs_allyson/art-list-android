package com.sofven.art_list.activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sofven.art_list.R;
import com.sofven.art_list.adapter.TermAdapter;
import com.sofven.art_list.adapter.ViewPagerAdapter;
import com.sofven.art_list.client.ApiClient;
import com.sofven.art_list.enums.ResponseStatus;
import com.sofven.art_list.fragment.FeedFragment;
import com.sofven.art_list.model.Term;
import com.sofven.art_list.util.AnalyticsUtil;
import com.sofven.art_list.util.NetworkUtil;
import com.sofven.art_list.result.TermResult;
import com.sofven.art_list.util.StringUtil;

public class TermPostsActivity extends AppCompatActivity {

    private static final String TAG = "ACTIVITY_TERM_POSTS";

    private Toolbar toolbar;
    private NetworkUtil networkUtil;
    private ApiClient apiClient;
    private ViewPagerAdapter adapter;
    private AnalyticsUtil analyticsUtil;
    private Tracker tracker;

    private ViewPager viewPager;

    private String type, slug;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_posts);

        // Initialize NetworkUtil class
        networkUtil = new NetworkUtil(TermPostsActivity.this);

        if (!networkUtil.isNetworkAvailable()) {
            finish();
        }

        // Get the Intent that started this activity
        Intent intent = getIntent();
        type = intent.getStringExtra(TermAdapter.EXTRA_TERM_TYPE);
        slug = intent.getStringExtra(TermAdapter.EXTRA_TERM_SLUG);

        if (slug == null || slug.isEmpty() || type == null || type.isEmpty()) {
            finish();
        }

        toolbar = findViewById(R.id.toolbar_term_posts);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Initialize Google analytics
        analyticsUtil = new AnalyticsUtil(TermPostsActivity.this);
        tracker = analyticsUtil.getDefaultTracker();

        // Initialize ApiClient
        apiClient = new ApiClient(TermPostsActivity.this);

        apiClient.getTerm(slug,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    // Convert json string to object
                    Gson gson = new GsonBuilder().create();
                    TermResult result = gson.fromJson(response, TermResult.class);
                    // If not successful
                    if (result.getStatus() != ResponseStatus.success) {
                        finish();
                    }
                    Term term = result.getTerm();

                    toolbar.setTitle(type.toUpperCase() + " - " + term.getName());

                    viewPager = findViewById(R.id.view_pager_term_posts);

                    Bundle bundle = new Bundle();
                    bundle.putString("slug", result.getTerm().getSlug());

                    FeedFragment fragment = new FeedFragment();
                    fragment.setArguments(bundle);

                    adapter = new ViewPagerAdapter(getSupportFragmentManager());
                    adapter.addFragment(fragment, getString(R.string.feed));
                    viewPager.setAdapter(adapter);

                    // Send hit to analytics
                    if (tracker != null) {
                        String screenName = TermPostsActivity.class.getSimpleName() + " - " + StringUtil.toCamelCase(type) + " - " + term.getName();
                        Log.d(MainActivity.ANALYTICS_TAG, screenName);
                        tracker.setScreenName(screenName);
                        tracker.send(new HitBuilders.ScreenViewBuilder().build());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    finish();
                }
            });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (adapter != null) {
            FeedFragment feedFragment = (FeedFragment) adapter.getItem(0);
            if (feedFragment != null) {
                feedFragment.refreshAdapter();
            }
        }
    }
}
