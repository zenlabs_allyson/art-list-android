package com.sofven.art_list.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.sofven.art_list.R;
import com.sofven.art_list.adapter.ViewPagerAdapter;
import com.sofven.art_list.fragment.PrivacyPolicyFragment;
import com.sofven.art_list.fragment.TermsOfServiceFragment;
import com.sofven.art_list.util.AnalyticsUtil;

public class LegalActivity extends AppCompatActivity {

    private static final String TAG = "ACTIVITY_LEGAL";

    private AnalyticsUtil analyticsUtil;
    private Tracker tracker;
    private ViewPagerAdapter adapter;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);

        toolbar = findViewById(R.id.toolbar_legal);
        tabLayout = findViewById(R.id.tab_layout_legal);
        viewPager = findViewById(R.id.view_pager_legal);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Initialize Google analytics
        analyticsUtil = new AnalyticsUtil(LegalActivity.this);
        tracker = analyticsUtil.getDefaultTracker();

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PrivacyPolicyFragment(), getString(R.string.privacy_policy));
        adapter.addFragment(new TermsOfServiceFragment(), getString(R.string.terms_of_service));
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        // Get the Intent that started this activity
        Intent intent = getIntent();
        int index = intent.getIntExtra(MainActivity.EXTRA_LEGAL_TYPE, 0);

        viewPager.setCurrentItem(index);

        // Send hit to analytics
        sendAnalyticsHit();

        // Add on page change listener for viewPager
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // Send hit to analytics
                sendAnalyticsHit();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private void sendAnalyticsHit() {
        if (tracker != null) {
            String screenName = LegalActivity.class.getSimpleName() + " - " + adapter.getPageTitle(viewPager.getCurrentItem()).toString();
            Log.d(MainActivity.ANALYTICS_TAG, screenName);
            tracker.setScreenName(screenName);
            tracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }
}
