package com.sofven.art_list.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.sofven.art_list.R;
import com.sofven.art_list.adapter.ViewPagerAdapter;
import com.sofven.art_list.fragment.ArtistFragment;
import com.sofven.art_list.fragment.FeedFragment;
import com.sofven.art_list.util.AnalyticsUtil;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = "ACTIVITY_SEARCH";

    private ViewPagerAdapter adapter;
    private Toolbar toolbar;
    private AnalyticsUtil analyticsUtil;
    private Tracker tracker;

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private SearchView searchView;

    private FeedFragment feedFragment;
    private ArtistFragment artistFragment;

    private String search = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        toolbar = findViewById(R.id.toolbar_search);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = findViewById(R.id.view_pager_search);
        tabLayout = findViewById(R.id.tab_layout_search);

        Bundle bundle = new Bundle();
        bundle.putString("search", search);

        feedFragment = new FeedFragment();
        artistFragment = new ArtistFragment();

        feedFragment.setArguments(bundle);
        artistFragment.setArguments(bundle);

        // Initialize Google analytics
        analyticsUtil = new AnalyticsUtil(SearchActivity.this);
        tracker = analyticsUtil.getDefaultTracker();

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(feedFragment, getString(R.string.feed));
        adapter.addFragment(artistFragment, getString(R.string.artists));
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        searchView = findViewById(R.id.search_view_search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search = query;
                // Send hit to analytics
                sendAnalyticsHit();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search = newText;

                Bundle bundle = new Bundle();
                bundle.putString("search", search);

                feedFragment = new FeedFragment();
                artistFragment = new ArtistFragment();

                feedFragment.setArguments(bundle);
                artistFragment.setArguments(bundle);

                adapter.clear();
                adapter.addFragment(feedFragment, getString(R.string.feed));
                adapter.addFragment(artistFragment, getString(R.string.artists));
                viewPager.setAdapter(adapter);
                return false;
            }
        });

        // Add on page change listener for viewPager
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // Send hit to analytics
                sendAnalyticsHit();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Send hit to analytics
        sendAnalyticsHit();

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        searchView.requestFocus();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (adapter != null) {
            FeedFragment feedFragment = (FeedFragment) adapter.getItem(0);
            if (feedFragment != null) {
                feedFragment.refreshAdapter();
            }
        }
    }

    private void sendAnalyticsHit() {
        if (tracker != null) {
            String screenName = SearchActivity.class.getSimpleName() + " - " + adapter.getPageTitle(viewPager.getCurrentItem()).toString();
            if (search != null && !search.isEmpty()) {
                screenName = screenName + " - Query: " + search;
            }
            Log.d(MainActivity.ANALYTICS_TAG, screenName);
            tracker.setScreenName(screenName);
            tracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }
}
