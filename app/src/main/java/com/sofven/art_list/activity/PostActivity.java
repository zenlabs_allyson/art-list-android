package com.sofven.art_list.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sofven.art_list.R;
import com.sofven.art_list.adapter.PostAdapter;
import com.sofven.art_list.adapter.TermAdapter;
import com.sofven.art_list.client.ApiClient;
import com.sofven.art_list.enums.PostReaction;
import com.sofven.art_list.enums.PostTermType;
import com.sofven.art_list.enums.PreferenceName;
import com.sofven.art_list.enums.ResponseStatus;
import com.sofven.art_list.model.Term;
import com.sofven.art_list.util.AnalyticsUtil;
import com.sofven.art_list.util.NetworkUtil;
import com.sofven.art_list.model.Post;
import com.sofven.art_list.model.PostFile;
import com.sofven.art_list.result.PostResult;
import com.sofven.art_list.result.RestResult;

import java.util.ArrayList;
import java.util.List;

public class PostActivity extends AppCompatActivity {

    private static final String TAG = "ACTIVITY_POST";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Toolbar toolbar;
    private ApiClient apiClient;
    private NetworkUtil networkUtil;
    private Resources res;
    private AnalyticsUtil analyticsUtil;
    private Tracker tracker;

    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout files;
    private TextView content, message;
    private FlexboxLayout artists;
    private ImageButton react;

    private String slug, reaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        // Initialize NetworkUtil class
        networkUtil = new NetworkUtil(PostActivity.this);

        if (!networkUtil.isNetworkAvailable()) {
            finish();
        }

        // Get the Intent that started this activity
        Intent intent = getIntent();
        slug = intent.getStringExtra(PostAdapter.EXTRA_POST_SLUG);

        if (slug == null || slug.isEmpty()) {
            finish();
        }

        res = getResources();

        toolbar = findViewById(R.id.toolbar_post);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        // Initialize SharedPreferences
        preferences = PostActivity.this.getSharedPreferences(PreferenceName.POSTS_SETTINGS.name(), Context.MODE_PRIVATE);
        editor = preferences.edit();

        content = findViewById(R.id.text_view_post_content);
        files = findViewById(R.id.layout_post_files);
        message = findViewById(R.id.text_view_post_message);
        artists = findViewById(R.id.flexbox_post_terms_artists);
        react = findViewById(R.id.image_button_post_react);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_post);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initializePost();
            }
        });

        initializePost();

        // Initialize Google analytics
        analyticsUtil = new AnalyticsUtil(PostActivity.this);
        tracker = analyticsUtil.getDefaultTracker();

    }

    private void initializePost() {

        if (!networkUtil.isNetworkAvailable()) {
            swipeRefreshLayout.setRefreshing(false);
            message.setText(R.string.no_network);
            message.setVisibility(View.VISIBLE);
            return;
        }

        // Initialize ApiClient
        apiClient = new ApiClient(PostActivity.this);

        apiClient.getPost(slug,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    // Convert json string to object
                    Gson gson = new GsonBuilder().create();
                    PostResult result = gson.fromJson(response, PostResult.class);
                    // If not successful
                    if (result.getStatus() != ResponseStatus.success) {
                        message.setText(result.getMessage());
                        message.setVisibility(View.VISIBLE);
                        return;
                    }
                    message.setText("");
                    message.setVisibility(View.INVISIBLE);

                    final Post post = result.getPost();
                    toolbar.setTitle(post.getName());
                    content.setText(post.getContent());

                    content.setVisibility(View.GONE);
                    if (post.getContent() != null && !post.getContent().isEmpty()) {
                        content.setVisibility(View.VISIBLE);
                    }

                    artists.removeAllViews();
                    List<Button> artistButtons = new ArrayList<>();
                    if (post.getTerms().size() > 0) {
                        for (final Term term : post.getTerms()) {
                            if (term.getType().equals(PostTermType.artists.name())) {
                                LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                int spacingHalf = (int) getResources().getDimension(R.dimen.spacing_half);
                                buttonLayoutParams.setMargins(spacingHalf,spacingHalf,spacingHalf,spacingHalf);
                                Button artistButton = new Button(PostActivity.this);
                                artistButton.setLayoutParams(buttonLayoutParams);
                                artistButton.setText(term.getName());
                                artistButton.setOnClickListener(new View.OnClickListener() {
                                     @Override
                                     public void onClick(View view) {
                                         Intent intent = new Intent(PostActivity.this, TermPostsActivity.class);
                                         intent.putExtra(TermAdapter.EXTRA_TERM_TYPE, term.getType());
                                         intent.putExtra(TermAdapter.EXTRA_TERM_SLUG, term.getSlug());
                                         startActivity(intent);
                                     }
                                 });
                                artistButtons.add(artistButton);
                            }
                        }
                    }

                    if (artistButtons.size() > 0) {
                        TextView artistsLabel = new TextView(PostActivity.this);
                        artistsLabel.setText(getString(R.string.string_with_colon, getString(R.string.artists)));

                        artists.addView(artistsLabel);
                        for (Button aButton : artistButtons) {
                            artists.addView(aButton);
                        }
                    }

                    files.removeAllViews();
                    if (post.getFiles().size() > 0) {
                        for (final PostFile file : post.getFiles()) {
                            final ImageView image = new ImageView(PostActivity.this);
                            LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            imageParams.setMargins(0, 0, 0, res.getDimensionPixelSize(R.dimen.spacing_one));
                            image.setLayoutParams(imageParams);
                            image.setAdjustViewBounds(true);
                            Glide.with(PostActivity.this)
                                .load(file.getFile())
                                //.placeholder(R.drawable.placeholder)
                                .thumbnail(Glide.with(PostActivity.this).load(R.drawable.loading))
                                .into(image);
                            image.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    final Dialog dialog = new Dialog(PostActivity.this, android.R.style.Theme_Black_NoTitleBar);
                                    dialog.setContentView(R.layout.dialog_post_file);
                                    dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                                    PhotoView photoView;
                                    TextView name, description;
                                    ImageButton closeDialog;

                                    photoView = dialog.findViewById(R.id.photo_view_dialog_post_file);
                                    name = dialog.findViewById(R.id.text_view_dialog_post_file_name);
                                    description = dialog.findViewById(R.id.text_view_dialog_post_file_description);
                                    closeDialog = dialog.findViewById(R.id.button_dialog_close);

                                    LinearLayout.LayoutParams photoViewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    photoView.setLayoutParams(photoViewParams);
                                    photoView.setImageDrawable(image.getDrawable());
                                    photoView.setAdjustViewBounds(true);
                                    photoView.setScaleType(ImageView.ScaleType.FIT_CENTER);

                                    name.setText(file.getName());
                                    description.setText(file.getDescription());
//
//                                    Glide.with(PostActivity.this)
//                                        .load(file.getFile())
//                                        .placeholder(R.drawable.placeholder)
//                                        .thumbnail(Glide.with(PostActivity.this).load(R.drawable.loading))
//                                        .into(photoView);

                                    closeDialog.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.show();
                                }
                            });
                            files.addView(image);
                        }
                    }

                    Boolean liked = preferences.getBoolean(PostReaction.like.name() + post.getId(), false);
                    if (liked) {
                        react.setImageResource(R.drawable.ic_like_filled);
                    } else {
                        react.setImageResource(R.drawable.ic_like);
                    }

                    react.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Boolean liked = preferences.getBoolean(PostReaction.like.name() + post.getId(), false);
                            if (liked) {
                                reaction = PostReaction.dislike.name();
                            } else {
                                reaction = PostReaction.like.name();
                            }
                            apiClient.postReact(slug, reaction,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // Convert json string to object
                                        Gson gson = new GsonBuilder().create();
                                        RestResult result = gson.fromJson(response, RestResult.class);
                                        if (result.getStatus() != ResponseStatus.success) {
                                            Toast.makeText(PostActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        // Success
                                        if (reaction.equals(PostReaction.like.name())) {
                                            react.setImageResource(R.drawable.ic_like_filled);
                                            editor.putBoolean(PostReaction.like.name() + post.getId(), true).commit();
                                            reaction = PostReaction.dislike.name();
                                        } else {
                                            react.setImageResource(R.drawable.ic_like);
                                            editor.remove(PostReaction.like.name() + post.getId()).commit();
                                            reaction = PostReaction.like.name();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(PostActivity.this, getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                                    }
                                });
                        }
                    });
                    swipeRefreshLayout.setRefreshing(false);

                    // Send hit to analytics
                    if (tracker != null) {
                        String screenName = PostActivity.class.getSimpleName() + " - " + post.getName();
                        Log.d(MainActivity.ANALYTICS_TAG, screenName);
                        tracker.setScreenName(screenName);
                        tracker.send(new HitBuilders.ScreenViewBuilder().build());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    message.setText(getString(R.string.error_occurred));
                    message.setVisibility(View.VISIBLE);
                }
            });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

}
