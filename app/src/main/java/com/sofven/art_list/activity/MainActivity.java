package com.sofven.art_list.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.sofven.art_list.R;
import com.sofven.art_list.adapter.ViewPagerAdapter;
import com.sofven.art_list.enums.PostOrder;
import com.sofven.art_list.enums.PostsSettings;
import com.sofven.art_list.enums.PreferenceName;
import com.sofven.art_list.fragment.ArtistFragment;
import com.sofven.art_list.fragment.FeedFragment;
import com.sofven.art_list.util.AnalyticsUtil;
import com.sofven.art_list.util.NetworkUtil;
import com.sofven.art_list.util.StringUtil;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "ACTIVITY_MAIN";
    public static final String ANALYTICS_TAG = "ANALYTICS_SCREEN";
    public static final String EXTRA_LEGAL_TYPE = "LEGAL_PAGE_INDEX";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ViewPagerAdapter adapter;
    private Resources res;
//    private NetworkUtil networkUtil;
    private AnalyticsUtil analyticsUtil;
    private Tracker analyticsTracker;

    private AlertDialog.Builder alertBuilder;
    private AlertDialog alertConfirm;

    private DrawerLayout drawerLayout;
    private ViewPager viewPager;
//    private TabLayout tabLayout;
    private ImageButton toolbarButtonMenu, toolbarButtonSearch;
    private NavigationView navigationView;
    private ImageButton navHeaderButtonMenu;
    private Button buttonRandom, buttonRecent;
    private TextView privacyPolicy, termsOfService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        res = getResources();

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbarButtonMenu = findViewById(R.id.toolbar_main_button_menu);
        toolbarButtonSearch = findViewById(R.id.toolbar_main_button_search);

        navHeaderButtonMenu = navigationView.getHeaderView(0).findViewById(R.id.nav_header_button_menu);
        buttonRandom = navigationView.getHeaderView(0).findViewById(R.id.nav_header_button_random);
        buttonRecent = navigationView.getHeaderView(0).findViewById(R.id.nav_header_button_recent);

//        tabLayout = findViewById(R.id.tab_layout_main);
        viewPager = findViewById(R.id.view_pager_main);

        privacyPolicy = findViewById(R.id.text_view_privacy_policy);
        termsOfService = findViewById(R.id.text_view_terms_of_service);

        // Initialize SharedPreferences
        preferences = MainActivity.this.getSharedPreferences(PreferenceName.POSTS_SETTINGS.name(), Context.MODE_PRIVATE);
        editor = preferences.edit();

        // Set drawer layout listeners
        drawerLayout.addDrawerListener(
            new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {
                    // Respond when the drawer's position changes
                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    // Respond when the drawer is opened
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    // Respond when the drawer is closed
                }

                @Override
                public void onDrawerStateChanged(int newState) {
                    // Respond when the drawer motion state changes
                }
            }
        );

        // Initialize Google analytics
        analyticsUtil = new AnalyticsUtil(MainActivity.this);
        analyticsTracker = analyticsUtil.getDefaultTracker();

        // Initialize Network Util
//        networkUtil = new NetworkUtil(MainActivity.this);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FeedFragment(), getString(R.string.feed));
        adapter.addFragment(new ArtistFragment(), getString(R.string.artists));
        viewPager.setAdapter(adapter);
//        tabLayout.setupWithViewPager(viewPager);

//        TextView tabOne = (TextView) LayoutInflater.from(MainActivity.this).inflate(R.layout.layout_tab, null);
//        tabOne.setText(tabLayout.getTabAt(0).getText());
//        tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_dashboard_black_24dp, 0, 0, 0);
//
//        TextView tabTwo = (TextView) LayoutInflater.from(MainActivity.this).inflate(R.layout.layout_tab, null);
//        tabTwo.setText(tabLayout.getTabAt(1).getText());
//        tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_palette_black_24dp, 0, 0, 0);
//
//        tabLayout.getTabAt(0).setCustomView(tabOne);
//        tabLayout.getTabAt(1).setCustomView(tabTwo);
//
//        tabLayout.getTabAt(0).setIcon(R.drawable.ic_dashboard_black_24dp);
//        tabLayout.getTabAt(1).setIcon(R.drawable.ic_palette_black_24dp);

        // Setup navigation drawer
        navigationView.setNavigationItemSelectedListener(
            new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {
                    // close drawer when item is tapped
                    drawerLayout.closeDrawers();
                    // When menu item is selected
                    switch (menuItem.getItemId()) {
                        case R.id.nav_feed:
                            viewPager.setCurrentItem(0);
                            break;
                        case R.id.nav_artists:
                            viewPager.setCurrentItem(1);
                            break;
                    }
                    return true;
                }
            });

        // Add on page change listener for viewPager
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // Set current active menu item
                navigationView.getMenu().getItem(position).setChecked(true);
                // Send hit to analytics
                sendAnalyticsHit();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Set current active menu item to first index
        navigationView.getMenu().getItem(0).setChecked(true);

        toolbarButtonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });

        navHeaderButtonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
            }
        });

        toolbarButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });

        setButtonStatus();

        buttonRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Put post order to shared preference
                editor.putString(PostsSettings.ORDER.name(), PostOrder.RANDOM.name());
                editor.apply();
                // Refresh view pager by resetting adapter
                viewPager.setAdapter(null);
                viewPager.setAdapter(adapter);
                // Set current active menu item to first index
                navigationView.getMenu().getItem(0).setChecked(true);
                // Set button status
                setButtonStatus();
                drawerLayout.closeDrawers();
                sendAnalyticsHit();
            }
        });

        buttonRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Put post order to shared preference
                editor.putString(PostsSettings.ORDER.name(), PostOrder.RECENT.name());
                editor.apply();
                // Refresh view pager by resetting adapter
                viewPager.setAdapter(null);
                viewPager.setAdapter(adapter);
                // Set current active menu item to first index
                navigationView.getMenu().getItem(0).setChecked(true);
                // Set button status
                setButtonStatus();
                drawerLayout.closeDrawers();
                sendAnalyticsHit();
            }
        });

        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LegalActivity.class);
                intent.putExtra(EXTRA_LEGAL_TYPE, 0);
                startActivity(intent);
            }
        });

        termsOfService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LegalActivity.class);
                intent.putExtra(EXTRA_LEGAL_TYPE, 1);
                startActivity(intent);
            }
        });

        // Build alert dialog for app exit confirmation
        alertBuilder = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialog);
        alertBuilder.setMessage(getString(R.string.exit_confirmation));
        alertBuilder.setCancelable(true);
        alertBuilder.setPositiveButton(
            getString(R.string.yes),
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    finish();
                }
            });
        alertBuilder.setNegativeButton(
            getString(R.string.no),
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
        alertConfirm = alertBuilder.create();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        alertConfirm.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            FeedFragment feedFragment = (FeedFragment) adapter.getItem(0);
            if (feedFragment != null) {
                feedFragment.refreshAdapter();
            }
        }
        sendAnalyticsHit();
    }

    private void setButtonStatus() {
        preferences = MainActivity.this.getSharedPreferences(PreferenceName.POSTS_SETTINGS.name(), Context.MODE_PRIVATE);
        String postOrder = preferences.getString(PostsSettings.ORDER.name(), PostOrder.RANDOM.name());
        // Change button text color and background based on value of order in preference
        if (postOrder.equals(PostOrder.RECENT.name())) {
            buttonRecent.setTextColor(res.getColor(R.color.colorPrimary));
            buttonRecent.setBackgroundResource(R.drawable.button_primary);
            buttonRandom.setTextColor(res.getColor(R.color.colorAccent));
            buttonRandom.setBackgroundColor(res.getColor(R.color.colorPrimary));
        } else {
            buttonRandom.setTextColor(res.getColor(R.color.colorPrimary));
            buttonRandom.setBackgroundResource(R.drawable.button_primary);
            buttonRecent.setTextColor(res.getColor(R.color.colorAccent));
            buttonRecent.setBackgroundColor(res.getColor(R.color.colorPrimary));
        }
    }

    private void sendAnalyticsHit() {
        if (analyticsTracker != null && viewPager != null) {
            String screenName = MainActivity.class.getSimpleName() + " - " + adapter.getPageTitle(viewPager.getCurrentItem()).toString();
            // If in feed fragment
            if (viewPager.getCurrentItem() == 0) {
                String postOrder = preferences.getString(PostsSettings.ORDER.name(), PostOrder.RANDOM.name());
                screenName = screenName + " " + StringUtil.toCamelCase(postOrder);
            }
            Log.d(MainActivity.ANALYTICS_TAG, screenName);
            analyticsTracker.setScreenName(screenName);
            analyticsTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }

}
