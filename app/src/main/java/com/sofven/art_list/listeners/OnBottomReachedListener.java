package com.sofven.art_list.listeners;

public interface OnBottomReachedListener {

    void onBottomReached(int position);

}